#include <iostream>
using namespace std;

int bruteForce(int *h, int n) {
	int max = -1, sum, stdNum;
	int i, j;

	for (i = 0; i < n; i++) {
		sum = 0;
		stdNum = h[i];

		for (j = i - 1; j >= 0; j--) {
			if (h[j] < stdNum)
				break;
			sum += stdNum;
		}
		for (j = i; j < n; j++) {
			if (h[j] < stdNum)
				break;
			sum += stdNum;
		}
		
		if (max < sum) {
			max = sum;
		}
	}

	return max;
}

int main() {
	std::ios::sync_with_stdio(false);
	int C, N, h[20000];
	int k, i;

	cin >> C;

	for (k = 0; k < C; k++) {
		cin >> N;
		for (i = 0; i<N; i++) {
			cin >> h[i];
		}

		cout << bruteForce(h, N) << endl;
	}

	return 0;
}
