package main

import "fmt"

func main() {
	var C, N, L int
	var n []int

	fmt.Println("Num of Test Case")
	fmt.Scanln(&C)
	result := make([]float64, C)
	for j := 0; j < C; j++ {
		fmt.Println("N, L")

		fmt.Scanln(&N, &L)
		n = make([]int, N)

		fmt.Println("Price of Place")
		for i := 0; i < N; i++ {
			fmt.Scanf("%d", &n[i])
		}
		fmt.Scanln()

		var min float64
		for k := N; k >= L; k-- {
			for a := 0; a < N-k+1; a++ {
				sum := 0
				for b := 0; b < k; b++ {
					sum += n[a+b]
				}

				if a == 0 && k == N {
					min = float64(sum) / float64(k)
				}
				if float64(sum)/float64(k) < min {
					min = float64(sum) / float64(k)
				}

			}
		}

		result[j] = min
	}

	for _, v := range result {
		fmt.Println(v)
	}
}
