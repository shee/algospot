//https://programmers.co.kr/learn/courses/30/lessons/42895?language=go

func solution(N int, number int) int {
	var m [9]map[int]bool
	for i := 1; i < 9; i++ {
		m[i] = map[int]bool{}
	}

	for i, j := 1, N; i < 9; i, j = i+1, j*10+N {
		m[i][j] = true
	}

	for i := 2; i < 9; i++ {
		for j, k := 1, i-1; j <= k; j, k = j+1, k-1 {
			for v1 := range m[j] {
				for v2 := range m[k] {
					m[i][v1+v2] = true
					m[i][v1-v2] = true
					m[i][v2-v1] = true
					m[i][v1*v2] = true
					if v1 != 0 {
						m[i][v2/v1] = true
					}
					if v2 != 0 {
						m[i][v1/v2] = true
					}
				}
			}
		}
	}

	for i := 1; i < 9; i++ {
		if m[i][number] == true {
			return i
		}
	}

	return -1
}