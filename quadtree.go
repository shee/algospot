package main

import "fmt"

var ipt string
var input []byte

func main() {
	var C int

	fmt.Scanln(&C)
	for i := 0; i < C; i++ {
		fmt.Scanln(&ipt)
		input = []byte(ipt)
		reverse(0)
		fmt.Println(string(input))
	}
}

func replace(div []int, index int) {
	temp := make([]byte, len(input))
	copy(temp, input)

	temp = append(temp[:div[0]], input[div[2]:index+1]...)
	temp = append(temp, input[div[0]:div[2]]...)
	temp = append(temp, input[index+1:]...)

	copy(input, temp)
}

func reverse(index int) int {
	div := make([]int, 4)
	if input[index] == 'x' {
		for i := 0; i < 4; i++ {
			index++
			div[i] = index
			if input[index] == 'x' {
				index = reverse(index)
			}
		}

		replace(div, index)
	}

	return index
}
