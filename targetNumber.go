//https://programmers.co.kr/learn/courses/30/lessons/43165?language=go

func solution(numbers []int, target int) int {
	answer := 0

	answer += getNum(numbers[0], numbers[1:], target)
	answer += getNum(-numbers[0], numbers[1:], target)

	return answer
}

func getNum(num int, numbers []int, target int) int {
	result := 0

	if len(numbers) == 0 {
		if num == target {
			return 1
		}
		return 0
	}

	result += getNum(num+numbers[0], numbers[1:], target)
	result += getNum(num-numbers[0], numbers[1:], target)

	return result
}