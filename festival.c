#include <stdio.h>

int main() {
	int C, N, L, i, j, a, b, sum;
	int n[1000];
	double min, result[100];

	scanf("%d", &C);

	for (i = 0; i < C; i++) {
		scanf("%d %d", &N, &L);

		for (j = 0; j < N; j++) {
			scanf("%d", &n[j]);
		}

		for (j = N; j >= L; j--) {
			for (a = 0; a < N - j + 1; a++) {
				sum = 0;
				for (b = 0; b < j; b++) {
					sum += n[a + b];
				}

				if (a == 0 && j == N) {
					min = (double)sum / j;
				}
				if ((double)sum / j < min) {
					min = (double)sum / j;
				}
			}
		}

		result[i] = min;
	}
	
	for (i = 0; i < C; i++) {
		printf("%.11f\n", result[i]);
	}
}