#include <iostream>
#include <string>
#include <cstring>
#include <vector>
#include <algorithm>

using namespace std;

int cache[101][101];
string W, S;

int matchMemoized(int w, int s) {
    int& ret = cache[w][s];
    if(ret != -1) {
        return ret;
    }
    if(s < S.size() && w < W.size() && (W[w] == '?' || W[w] == S[s])) {
       return ret = matchMemoized(w+1, s+1);
    }
    if(w == W.size() && s==S.size()) {
        return ret = 1;
    }
    if(W[w] == '*') {
        if(matchMemoized(w+1, s) || s < S.size() && matchMemoized(w, s+1))
            return ret = 1;
    }

    return ret = 0;
}

int main() {
    int c, n;

    cin >> c;
    for (int i = 0; i < c; i++) {
        vector<string> v;
        cin >> W;
        cin >> n;
        for (int j = 0; j < n; j++) {
            memset(cache, -1, sizeof(cache));
            cin >> S;
            if(matchMemoized(0, 0) == 1) {
                v.push_back(S);
            }
        }

        sort(v.begin(), v.end());
        for (int j = 0; j < v.size(); j++) {
            cout << v[j] << endl;
        }
    }    
    
    return 0;
}